//
//  InterfaceController.swift
//  Charged WatchKit Extension
//
//  Created by Yaakov Gamliel on 6/25/15.
//  Copyright (c) 2015 G2Think. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    //MARK: Outlets

    @IBOutlet weak var batteryLevelLabel: WKInterfaceLabel!
    @IBOutlet weak var batteryStatusImage: WKInterfaceImage!
    @IBOutlet weak var batteryLevelPhoneImage: WKInterfaceImage!
    
    //MARK: Interface controller methods
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        
        configureBatteryIcons()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        
        batteryStatusImage.setImageNamed("")
        batteryLevelLabel.setText("")
        batteryLevelPhoneImage.setImageNamed("")
    }
    
    //MARK: Helper methods

    func configureBatteryIcons() {
        let myDevice = UIDevice.currentDevice()
        myDevice.batteryMonitoringEnabled = true
        
        let batteryLevel = myDevice.batteryLevel * 100
        
        batteryLevelLabel.setText("\(Int(batteryLevel))%")
        
        let batteryState = myDevice.batteryState
        
        if batteryLevel < 20.0 {
            batteryStatusImage.setImageNamed("chargeme.png")
        }
        else {
            batteryStatusImage.setImageNamed(batteryStateImageName(batteryState))
        }
        
        
        batteryLevelPhoneImage.setImageNamed(phoneImageForLevel(batteryLevel))
    }
    
    func batteryStateImageName(batteryState: UIDeviceBatteryState) -> String? {
        switch batteryState {
            
        case UIDeviceBatteryState.Unknown:
            return nil
        case UIDeviceBatteryState.Unplugged:
            return nil
        case UIDeviceBatteryState.Charging:
            return "charging.png"
        case UIDeviceBatteryState.Full:
            return "charging.png"
        default:
            return nil
        }
    }
    
    func phoneImageForLevel(batteryLevel: Float) -> String {
        switch batteryLevel {
       
        case 0..<20:
            return "10phone.png"
        case 20..<30:
            return "20phone.png"
        case 30..<40:
            return "30phone.png"
        case 40..<50:
            return "40phone.png"
        case 50..<60:
            return "50phone.png"
        case 60..<70:
            return "60phone.png"
        case 70..<80:
            return "70phone.png"
        case 80..<90:
            return "80phone.png"
        case 90..<99:
            return "90phone.png"
        case 99..<100:
            return "100phone.png"
        default:
            return "100phone" // This is TMP
        }
    }
}
