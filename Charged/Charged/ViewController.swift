//
//  ViewController.swift
//  Charged
//
//  Created by Yaakov Gamliel on 6/25/15.
//  Copyright (c) 2015 G2Think. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: Outlets
    
    @IBOutlet weak var batteryStuffViewContainer: UIView!
    @IBOutlet var contentScrollView: UIScrollView!
    @IBOutlet weak var batteryLevelImage: UIImageView!
    @IBOutlet weak var batteryStatusImage: UIImageView!
    
    //MARK: ViewController methods
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        configureScrollView(self.contentScrollView)
        
        setupBatteryInfoItems()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        
        return UIStatusBarStyle.LightContent
    }
    
    //MARK: Helper methods
    
    func configureScrollView(contentScrollView: UIScrollView) {
        contentScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.height + 280)
        contentScrollView.showsVerticalScrollIndicator = false
    }
    
    func setupBatteryInfoItems() {
        let myDevice = UIDevice.currentDevice()
        myDevice.batteryMonitoringEnabled = true
        
        let batteryLevel = myDevice.batteryLevel * 100
        
        let batteryState = myDevice.batteryState
        
        if batteryLevel <= 20.0 {
            batteryStatusImage.image = UIImage(named: "chargeme.png")
        }
        else {
            batteryStatusImage.image = UIImage(named: batteryStateImageName(batteryState))
        }
        
        batteryStuffViewContainer.backgroundColor = configureContainerColor(batteryLevel)
        
        batteryLevelImage.image = UIImage(named: phoneImageForLevel(batteryLevel))
    }
    
    func configureContainerColor(batteryLevel: Float) -> UIColor {
        
        if batteryLevel <= 20.0 {
            
            return UIColor.lowBatteryLevelColor()
        }
        else {
            
            return UIColor.normalBatteryLevelColor()
        }
    }
    
    func batteryStateImageName(batteryState: UIDeviceBatteryState) -> String {
        switch batteryState {
            
        case UIDeviceBatteryState.Unknown:
            return ""
        case UIDeviceBatteryState.Unplugged:
            return ""
        case UIDeviceBatteryState.Charging:
            return "charging.png"
        case UIDeviceBatteryState.Full:
            return "charging.png"
        default:
            return ""
        }
    }
    
    func phoneImageForLevel(batteryLevel: Float) -> String {
        switch batteryLevel {
       
        case 0..<20:
            return "10phone.png"
        case 20..<30:
            return "20phone.png"
        case 30..<40:
            return "30phone.png"
        case 40..<50:
            return "40phone.png"
        case 50..<60:
            return "50phone.png"
        case 60..<70:
            return "60phone.png"
        case 70..<80:
            return "70phone.png"
        case 80..<90:
            return "80phone.png"
        case 90..<99:
            return "90phone.png"
        case 99..<100:
            return "100phone.png"
        default:
            return "100phone" // This is TMP
        }
    }
}