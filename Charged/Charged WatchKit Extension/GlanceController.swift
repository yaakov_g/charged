//
//  GlanceController.swift
//  Batero WatchKit Extension
//
//  Created by Yaakov Gamliel on 6/22/15.
//  Copyright (c) 2015 G2Think. All rights reserved.
//

import WatchKit
import Foundation


class GlanceController: WKInterfaceController {

    //MARK: Outlets

    @IBOutlet weak var batteryLevelLabel: WKInterfaceLabel!
    
    @IBOutlet weak var batteryStateImage: WKInterfaceImage!
    
    @IBOutlet weak var batteryLevelImage: WKInterfaceImage!
    
    //MARK: Interface controller methods

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
                
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        configureBatteryIcons()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        
        batteryStateImage.setImageNamed("")
        batteryLevelImage.setImageNamed("")
    }
    
    //MARK: Helper methods

    func configureBatteryIcons() {
        let myDevice = UIDevice.currentDevice()
        myDevice.batteryMonitoringEnabled = true
        
        let batteryLevel = myDevice.batteryLevel * 100
        
        batteryLevelLabel.setText("\(Int(batteryLevel))%")
        
        let batteryState = myDevice.batteryState
        
        if batteryLevel < 20.0 {
            batteryStateImage.setImageNamed("chargeme.png")
        }
        else {
            batteryStateImage.setImageNamed(batteryStateImageName(batteryState))
        }
        
        batteryLevelImage.setImageNamed(batteryImageForLevel(batteryLevel))
    }
    
    func batteryStateLabel(batteryState: UIDeviceBatteryState) -> String {
       
        switch batteryState {
            
        case UIDeviceBatteryState.Unknown:
            return "Unknown"
        case UIDeviceBatteryState.Unplugged:
            return "Unplugged"
        case UIDeviceBatteryState.Charging:
            return "Charging"
        case UIDeviceBatteryState.Full:
            return "Full"
        default:
            return "Unknown"
        }
    }
    
    func batteryStateImageName(batteryState: UIDeviceBatteryState) -> String? {
        switch batteryState {
            
        case UIDeviceBatteryState.Unknown:
            return nil
        case UIDeviceBatteryState.Unplugged:
            return nil
        case UIDeviceBatteryState.Charging:
            return "charging.png"
        case UIDeviceBatteryState.Full:
            return "charging.png"
        default:
            return nil
        }
    }
    
    func batteryImageForLevel(batteryLevel: Float) -> String {
        switch batteryLevel {
        
        case 0..<20:
            return "10batG.png"
        case 20..<30:
            return "20batG.png"
        case 30..<40:
            return "30batG.png"
        case 40..<50:
            return "40batG.png"
        case 50..<60:
            return "50batG.png"
        case 60..<70:
            return "60batG.png"
        case 70..<80:
            return "70batG.png"
        case 80..<90:
            return "80batG.png"
        case 90..<99:
            return "90batG.png"
        case 99..<100:
            return "100batG.png"
        default:
            return "100batG" // This is TMP
        }
    }
    
}
