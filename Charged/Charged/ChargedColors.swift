//
//  ChargedColors.swift
//  Charged
//
//  Created by Yaakov Gamliel on 7/3/15.
//  Copyright (c) 2015 G2Think. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func normalBatteryLevelColor() -> UIColor {
        return UIColor(red: 0.408, green: 0.898, blue: 0.667, alpha: 1.00)
    }
    
    class func lowBatteryLevelColor() -> UIColor {
        return UIColor(red: 0.988, green: 0.267, blue: 0.259, alpha: 1.00)
    }
}
